use itertools::Itertools;

use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day14, part1)]
fn take_input(s: &str) -> Vec<String> {
    crate::shared::rotate(s)
        .lines()
        .map(|x| x.to_owned())
        .collect_vec()
}

#[aoc_generator(day14, part2)]
fn take_input2(s: &str) -> String {
    crate::shared::rotate(s)
}

#[aoc(day14, part1)]
fn part_one(v: &[String]) -> usize {
    v.iter().map(|x| move_north(x)).map(|x| load(&x)).sum()
}

#[aoc(day14, part2)]
fn part_two(s: &str) -> usize {
    let mut s = s.to_owned();
    for _ in 0..1000000000 {
        //NORTH
        s = s.lines().map(move_north).join("\n");
        s = crate::shared::rotate(&s);
        //WEST
        s = s.lines().map(move_north).join("\n");
        s = crate::shared::rotate(&s);
        //SOUTH
        s = s.lines().map(move_south).join("\n");
        s = crate::shared::rotate(&s);
        //EAST
        s = s.lines().map(move_south).join("\n");
        s = crate::shared::rotate(&s);
    }
    load(&s)
}

fn load(s: &str) -> usize {
    let mut max_load = s.len();
    s.chars()
        .map(|x| {
            let res = (max_load, x);
            max_load -= 1;
            res
        })
        .filter(|(_, x)| x == &'O')
        .map(|(idx, _)| idx)
        .sum()
}

fn move_north(s: &str) -> String {
    s.lines().map(move_north_line).join("\n")
}

fn move_south(s: &str) -> String {
    s.lines().map(move_south_line).join("\n")
}

fn move_north_line(s: &str) -> String {
    if s.contains('#') {
        s.split('#').map(move_north_line).join("#")
    } else {
        format!(
            "{}{}",
            s.chars().filter(|x| x == &'O').join(""),
            s.chars().filter(|x| x == &'.').join("")
        )
    }
}

fn move_south_line(s: &str) -> String {
    if s.contains('#') {
        s.split('#').map(move_south_line).join("#")
    } else {
        format!(
            "{}{}",
            s.chars().filter(|x| x == &'.').join(""),
            s.chars().filter(|x| x == &'O').join(""),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn rock() {
        assert_eq!(load("O"), 1);
    }

    #[test]
    fn no_rocks() {
        assert_eq!(load("."), 0);
    }

    #[test]
    fn rock_counts_max_load() {
        assert_eq!(load("O."), 2);
    }

    #[test]
    fn two_rocks() {
        assert_eq!(load("OO"), 3);
    }

    #[test]
    fn rock_moves() {
        assert_eq!(&move_north(".O"), "O.");
    }

    #[test]
    fn rock_doesnt_move() {
        assert_eq!(&move_north(".#O"), ".#O");
    }

    #[test]
    fn rock_moves_up_to_square_rock() {
        assert_eq!(&move_north(".O#..O.O"), "O.#OO...");
    }

    #[test]
    fn rock_moves_two_lines() {
        assert_eq!(&move_north("O\nO"), "O\nO");
    }
    const EXAMPLE: &str = "O....#....\nO.OO#....#\n.....##...\nOO.#O....O\n.O.....O#.\nO.#..O.#.#\n..O..#O..O\n.......O..\n#....###..\n#OO..#....";
    #[test]
    fn example_move() {
        assert_eq!(move_north(&crate::shared::rotate(EXAMPLE)), crate::shared::rotate("OOOO.#.O..\nOO..#....#\nOO..O##..O\nO..#.OO...\n........#.\n..#....#.#\n..O..#.O.O\n..O.......\n#....###..\n#....#...."));
    }

    #[test]
    fn example_part_one() {
        assert_eq!(part_one(&take_input(EXAMPLE)), 136);
    }

    /*
    #[test]
    fn example_part_two() {
        assert_eq!(part_two(&take_input2(EXAMPLE)), 64);
    }
    */
}
