use aoc_runner_derive::{aoc, aoc_generator};
#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<String> {
    input.lines().map(|s| s.to_owned()).collect()
}

fn into_number(x: u8) -> u8 {
    x - 48
}

fn keep_numbers(s: &str) -> Vec<u8> {
    s.chars()
        .map(|x| x.try_into().unwrap())
        .map(into_number)
        .filter(|x| x < &10)
        .collect()
}

fn replace_first_word(s: String) -> String {
    let mut idxs: [Option<usize>; 9] = [None; 9];
    idxs[0] = s.find("one");
    idxs[1] = s.find("two");
    idxs[2] = s.find("three");
    idxs[3] = s.find("four");
    idxs[4] = s.find("five");
    idxs[5] = s.find("six");
    idxs[6] = s.find("seven");
    idxs[7] = s.find("eight");
    idxs[8] = s.find("nine");
    let min_word = idxs
        .iter()
        .enumerate()
        .filter(|(_, x)| x.is_some())
        .min_by_key(|(_, x)| *x)
        .map(|(i, _)| i);

    match min_word {
        Some(0) => s.replace("one", "1"),
        Some(1) => s.replace("two", "2"),
        Some(2) => s.replace("three", "3"),
        Some(3) => s.replace("four", "4"),
        Some(4) => s.replace("five", "5"),
        Some(5) => s.replace("six", "6"),
        Some(6) => s.replace("seven", "7"),
        Some(7) => s.replace("eight", "8"),
        Some(8) => s.replace("nine", "9"),
        _ => s,
    }
}

fn replace_last_word(s: String) -> String {
    let mut idxs: [Option<usize>; 9] = [None; 9];
    idxs[0] = s.rfind("one");
    idxs[1] = s.rfind("two");
    idxs[2] = s.rfind("three");
    idxs[3] = s.rfind("four");
    idxs[4] = s.rfind("five");
    idxs[5] = s.rfind("six");
    idxs[6] = s.rfind("seven");
    idxs[7] = s.rfind("eight");
    idxs[8] = s.rfind("nine");
    let min_word = idxs
        .iter()
        .enumerate()
        .filter(|(_, x)| x.is_some())
        .max_by_key(|(_, x)| *x)
        .map(|(i, _)| i);

    match min_word {
        Some(0) => s.replace("one", "1"),
        Some(1) => s.replace("two", "2"),
        Some(2) => s.replace("three", "3"),
        Some(3) => s.replace("four", "4"),
        Some(4) => s.replace("five", "5"),
        Some(5) => s.replace("six", "6"),
        Some(6) => s.replace("seven", "7"),
        Some(7) => s.replace("eight", "8"),
        Some(8) => s.replace("nine", "9"),
        _ => s,
    }
}

#[aoc(day1, part2)]
fn part_two(v: &[String]) -> usize {
    let w: Vec<String> = v
        .iter()
        .map(|x| replace_first_word(x.clone()))
        .map(replace_last_word)
        .collect();

    part_one(&w)
}

#[aoc(day1, part1)]
fn part_one(v: &[String]) -> usize {
    v.iter()
        .map(|x| keep_numbers(x))
        .map(|x| (*x.first().unwrap() as usize * 10) + *x.last().unwrap() as usize)
        .sum::<usize>()
}

#[test]
fn example_1abc2() {
    assert_eq!(part_one(&input_generator("1abc2")), 12);
}

#[test]
fn example_pqr3stu8vwx() {
    assert_eq!(part_one(&input_generator("pqr3stu8vwx")), 38);
}

#[test]
fn example_one_two_three() {
    assert_eq!(part_two(&input_generator("onetwothree")), 13);
}

#[test]
fn replaces_the_first_instance_not_the_lowest_digit_() {
    assert_eq!(part_two(&input_generator("twone3")), 23);
}

#[test]
fn replaces_the_last_instance() {
    assert_eq!(part_two(&input_generator("nineeightsevensix")), 96);
}

#[test]
fn replaces_single_word() {
    assert_eq!(part_two(&input_generator("nine")), 99);
}

#[test]
fn examples_p2() {
    assert_eq!(part_two(&input_generator("two1nine")), 29);
    assert_eq!(part_two(&input_generator("eightwothree")), 83);
    assert_eq!(part_two(&input_generator("abcone2threexyz")), 13);
    assert_eq!(part_two(&input_generator("xtwone3four")), 24);
    assert_eq!(part_two(&input_generator("4nineeightseven2")), 42);
    assert_eq!(part_two(&input_generator("zoneight234")), 14);
    assert_eq!(part_two(&input_generator("7pqrstsixteen")), 76);

    assert_eq!(part_two(&input_generator("two1nine\neightwothree\nabcone2threexyz\nxtwone3four\n4nineeightseven2\nzoneight234\n7pqrstsixteen")), 281);
}
