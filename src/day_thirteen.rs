use crate::shared;
use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;

#[aoc_generator(day13)]
fn take_input(s: &str) -> Vec<String> {
    s.split("\n\n").map(|s| s.to_owned()).collect_vec()
}

#[aoc(day13, part1)]
fn part_one(blocks: &[String]) -> usize {
    vertical_matches(blocks, 0) + horizontal_matches(blocks, 0) * 100
}

#[aoc(day13, part2)]
fn part_two(blocks: &[String]) -> usize {
    horizontal_matches(blocks, 1) * 100 + vertical_matches(blocks, 1)
}

fn vertical_matches(blocks: &[String], allowed_errors: usize) -> usize {
    blocks
        .iter()
        .filter_map(|block| block_reflects_vertically(block, allowed_errors))
        .sum()
}

fn horizontal_matches(blocks: &[String], allowed_errors: usize) -> usize {
    blocks
        .iter()
        .map(|block| shared::rotate(block))
        .filter_map(|block| block_reflects_vertically(&block, allowed_errors))
        .sum()
}

fn block_reflects_vertically(s: &str, allowed_errors: usize) -> Option<usize> {
    (1..line_length(s)).find(|&at| all_lines_reflect_vertically(s, at, allowed_errors))
}

fn all_lines_reflect_vertically(s: &str, at: usize, allowed_errors: usize) -> bool {
    s.lines()
        .filter(|line| !reflects_vertically(line, at, allowed_errors))
        .count()
        == allowed_errors
}

fn reflects_vertically(s: &str, at: usize, allowed_errors: usize) -> bool {
    let (first, second) = s.split_at(at);
    let first = first.chars().rev();
    first.zip(second.chars()).filter(|(x, y)| x != y).count() <= allowed_errors
}

fn line_length(s: &str) -> usize {
    s.lines().next().unwrap().len()
}

#[cfg(test)]
mod test {

    use super::*;
    use rstest::rstest;
    const EXAMPLE: &str =
        "#.##..##\n..#.##.#.\n##......#\n##......#\n..#.##.#.\n..##..##.\n#.#.##.#.\n\n
#...##..#\n#....#..#\n..##..###\n#####.##\n#####.##\n..##..###\n#....#..#";

    #[test]
    fn test_example() {
        assert_eq!(part_one(&take_input(EXAMPLE)), 405);
    }

    /*
    #[test]
    fn test_example_part2() {
        assert_eq!(part_two(&take_input(EXAMPLE)), 400);
    }
    */

    #[rstest]
    #[case("##", 1)]
    #[case("..", 1)]
    #[case("##.", 1)]
    #[case(".##.", 2)]
    #[case(".##..", 2)]
    #[case("..##..", 3)]
    #[case(".##", 2)]
    fn test_reflects(#[case] case: &str, #[case] at: usize) {
        assert!(reflects_vertically(dbg!(case), at, 0));
    }

    #[rstest]
    #[case("..#", 2)]
    #[case("#.#", 1)]
    #[case(".#.#..", 2)]
    #[case(".#.#.", 2)]
    #[case("..#.#..", 3)]
    fn test_does_not_reflect(#[case] case: &str, #[case] at: usize) {
        assert!(!reflects_vertically(case, at, 0));
    }

    #[rstest]
    #[case("..\n##", Some(1))]
    #[case(".##\n.##", Some(2))]
    #[case("..#\n.##", None)]
    #[case(
        "#.##..##.\n..#.##.#.\n##......#\n##......#\n..#.##.#.\n..##..##.\n#.#.##.#.",
        Some(5)
    )]
    fn test_block_reflects_vertically(#[case] case: &str, #[case] at: Option<usize>) {
        assert_eq!(block_reflects_vertically(case, 0), at);
    }
    #[rstest]
    #[case(".\n.", "..")]
    #[case(".##\n.##\n##.", "..#\n###\n##.")]
    #[case(
        "#...##..#\n#....#..#\n..##..###\n#####.##.\n#####.##.\n..##..###\n#....#..#",
        "##.##.#\n...##..\n..####.\n..####.\n#..##..\n##....#\n..####.\n..####.\n###..##"
    )]
    fn test_rotate_block(#[case] case: &str, #[case] expected: &str) {
        assert_eq!(shared::rotate(case), expected);
    }
}
