use aoc_runner_derive::{aoc, aoc_generator};
#[aoc_generator(day2)]
pub fn input_generator(input: &str) -> Vec<Game> {
    input
        .lines()
        .map(|s| s.to_owned())
        .map(Game::from_string)
        .collect()
}

fn game_id(g: &Game) -> usize {
    g.id
}

#[aoc(day2, part1)]
fn day_two(input: &[Game]) -> usize {
    input.iter().filter(|x| x.is_valid()).map(game_id).sum()
}

impl From<&String> for Colour {
    fn from(s: &String) -> Self {
        let x: Vec<&str> = s.split(' ').collect();
        let v: u8 = x[0].parse().unwrap();
        match x[1] {
            "red" => Colour::Red(v),
            "blue" => Colour::Blue(v),
            _ => Colour::Green(v),
        }
    }
}

#[derive(Debug)]
enum Colour {
    Red(u8),
    Green(u8),
    Blue(u8),
}

impl Colour {
    fn is_valid(&self) -> bool {
        match self {
            Colour::Red(v) => v < &13,
            Colour::Green(v) => v < &14,
            Colour::Blue(v) => v < &15,
        }
    }
}

#[derive(Debug)]
pub struct Game {
    id: usize,
    observations: Vec<Vec<Colour>>,
}

impl Game {
    fn new(id: usize, observations: Vec<Vec<Colour>>) -> Self {
        Self { id, observations }
    }

    fn is_valid(&self) -> bool {
        self.observations
            .iter()
            .all(|o| o.iter().all(|x| x.is_valid()))
    }

    fn from_string(s: String) -> Self {
        let b: Vec<String> = s.split(':').map(|x| x.to_owned()).collect();
        let c: Vec<String> = b[1].split(';').map(|x| x.to_owned()).collect();
        let d: Vec<Vec<String>> = c
            .iter()
            .map(|s| s.split(',').map(|x| x.trim().to_owned()).collect())
            .collect();

        let e: Vec<Vec<Colour>> = d
            .iter()
            .map(|x| x.iter().map(|x| x.into()).collect())
            .collect();

        let f: usize = b[0].split(' ').collect::<Vec<&str>>()[1].parse().unwrap();

        Game::new(f, e)
    }
}
