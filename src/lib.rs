use aoc_runner_derive::aoc_lib;
mod day_eighteen;
mod day_eleven;
mod day_fifteen;
mod day_five;
mod day_four;
mod day_fourteen;
mod day_one;
mod day_seven;
mod day_six;
mod day_thirteen;
mod day_three;
mod day_twelve;
mod day_two;

mod shared {
    use itertools::Itertools;
    use std::collections::BTreeMap;

    pub fn rotate(s: &str) -> String {
        let mut map = BTreeMap::<usize, String>::new();
        s.lines().for_each(|s| {
            rotate_line(s, &mut map);
        });
        map_into_lines(map)
    }

    fn rotate_line(s: &str, map: &mut BTreeMap<usize, String>) {
        s.chars().enumerate().for_each(|(idx, c)| {
            map.entry(idx).or_default().push(c);
        });
    }

    fn map_into_lines(map: BTreeMap<usize, String>) -> String {
        map.into_values().join("\n")
    }
}

aoc_lib! {year=2023}
