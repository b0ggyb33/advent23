use itertools::Itertools;
use std::{collections::BTreeMap, str::FromStr};

#[aoc_runner_derive::aoc(day15, part1)]
fn parse(s: &str) -> usize {
    s.split(',').map(hash).sum()
}

#[aoc_runner_derive::aoc_generator(day15, part2)]
fn parse2(s: &str) -> Vec<Lens> {
    s.split(',').map_into::<Lens>().collect_vec()
}

fn part2(lenses: &[Lens]) -> usize {
    let mut hashmap: [BTreeMap<&str, Lens>; 256] = (0..256)
        .map(|_| BTreeMap::new())
        .collect_vec()
        .try_into()
        .unwrap();

    for lens in lenses.iter() {
        let lens_hash = lens.hash as usize;

        if let Some(length) = lens.focal_length {
            if hashmap[lens_hash].contains_key(&lens.label.as_str()) {
                hashmap[lens_hash].entry(&lens.label).and_modify(|x| {
                    x.focal_length.replace(length);
                });
            } else {
                hashmap[lens_hash as usize].insert(lens.label.as_str(), lens.clone());
            }
        } else {
            hashmap[lens_hash].remove(lens.label.as_str());
        }
    }

    hashmap
        .iter()
        .enumerate()
        .map(|(idx, map)| {
            map.values()
                .enumerate()
                .map(|(jdx, lens)| lens.focal_length.unwrap() * jdx + 1 * (idx) + 1)
                .sum::<usize>()
        })
        .sum()
}

impl PartialEq for Lens {
    fn eq(&self, other: &Self) -> bool {
        self.label == other.label
    }
}

fn hash(s: &str) -> usize {
    s.chars().map(|x| x as usize).fold(0, |acc, x| {
        let mut acc = acc;
        acc += x;
        acc *= 17;
        acc %= 256;
        acc
    })
}

#[derive(Debug, Clone)]
struct Lens {
    label: String,
    focal_length: Option<usize>,
    hash: u8,
}

#[derive(Debug)]
struct LensError;

impl From<&str> for Lens {
    fn from(s: &str) -> Self {
        s.parse().unwrap()
    }
}

impl FromStr for Lens {
    type Err = LensError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split = s.split('=').collect_vec();
        Ok(Lens {
            label: split
                .get(0)
                .ok_or(LensError {})
                .and_then(|x| Ok(x.to_string()))?,
            focal_length: split.get(1).and_then(|x| x.parse().ok()),
            hash: hash(s) as u8,
        })
    }
}

#[test]
fn HASH() {
    assert_eq!(hash("HASH"), 52);
}
