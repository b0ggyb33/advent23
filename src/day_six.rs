use aoc_runner_derive::{aoc, aoc_generator};
use nom::Parser;

#[aoc_generator(day6)]
fn take_input(s: &str) -> Vec<Race> {
    races(s).expect("valid parsing").1
}

#[aoc(day6, part1)]
fn part_one(v: &[Race]) -> usize {
    run_races(v)
}

#[aoc(day6, part2)]
fn part_two(_: &[Race]) -> usize {
    let race = Race::new(45988373, 295173412781210);
    count_winning_button_presses(&race)
}

fn count_winning_button_presses(race: &Race) -> usize {
    race.button_times()
        .map(|x| race.travelled(x))
        .filter(|&distance| race.beats_distance(distance))
        .count()
}

fn run_races(races: &[Race]) -> usize {
    races.iter().map(count_winning_button_presses).product()
}

#[derive(Debug)]
struct Race {
    time: usize,
    distance: usize,
}

impl Race {
    fn new(time: usize, distance: usize) -> Self {
        Self { time, distance }
    }
    fn travelled(&self, hold_time: usize) -> usize {
        hold_time * (self.time - hold_time)
    }
    fn beats_distance(&self, distance: usize) -> bool {
        distance > self.distance
    }

    fn button_times(&self) -> std::ops::Range<usize> {
        0..self.time
    }
}

fn races(s: &str) -> nom::IResult<&str, Vec<Race>> {
    nom::sequence::separated_pair(times, nom::character::complete::newline, distances)
        .map(|(times, distances): (Vec<_>, Vec<_>)| {
            times
                .iter()
                .zip(distances.iter())
                .map(|(&t, &d)| Race {
                    time: t as usize,
                    distance: d as usize,
                })
                .collect()
        })
        .parse(s)
}

fn times(s: &str) -> nom::IResult<&str, Vec<u32>> {
    line(s, "Time")
}

fn distances(s: &str) -> nom::IResult<&str, Vec<u32>> {
    line(s, "Distance")
}

fn line<'a>(s: &'a str, measurement: &str) -> nom::IResult<&'a str, Vec<u32>> {
    nom::sequence::preceded(
        nom::sequence::pair(
            nom::bytes::complete::tag(format!("{}: ", measurement).as_str()),
            nom::character::complete::space1,
        ),
        nom::multi::separated_list1(
            nom::character::complete::space1,
            nom::character::complete::u32,
        ),
    )(s)
}

#[test]
fn test_time() {
    assert_eq!(times("Time:     7 12 13").unwrap().1, vec![7, 12, 13]);
}

#[test]
fn test_distance() {
    assert_eq!(
        distances("Distance:     8 13 14").unwrap().1,
        vec![8, 13, 14]
    );
}

#[test]
fn example() {
    assert_eq!(
        run_races(&[Race::new(7, 9), Race::new(15, 40), Race::new(30, 200)]),
        288
    );
}
