use itertools::Itertools;
use std::collections::HashMap;

use aoc_runner_derive::{aoc, aoc_generator};

#[derive(Debug, Clone)]
pub struct Card {
    id: usize,
    winning_numbers: Vec<usize>,
    numbers: Vec<usize>,
}

impl Card {
    fn winning_numbers(&self) -> u32 {
        self.numbers
            .iter()
            .map(|x| self.winning_numbers.contains(x))
            .map_into::<u32>()
            .sum()
    }
    fn score(&self) -> u32 {
        2_u32.pow(self.winning_numbers() - 1)
    }

    fn winning_cards(&self) -> std::ops::Range<usize> {
        self.id + 1..self.id + 1 + self.winning_numbers() as usize
    }
}

impl std::str::FromStr for Card {
    type Err = std::io::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let colon = s.find(':').ok_or(Self::Err::new(
            std::io::ErrorKind::Other,
            "Finding colon failed",
        ))?;

        let id = card_number(&s[..colon])
            .map_err(|_| Self::Err::new(std::io::ErrorKind::Other, "Parsing Card number failed"))?;

        let (winning_numbers, numbers) = s[colon + 1..]
            .split('|')
            .map(numbers_from_str)
            .collect_tuple()
            .ok_or(Self::Err::new(
                std::io::ErrorKind::Other,
                "Parsing numbers failed",
            ))?;

        Ok(Self {
            id,
            winning_numbers,
            numbers,
        })
    }
}

fn card_number(s: &str) -> Result<usize, std::num::ParseIntError> {
    s.strip_prefix("Card ").unwrap_or_default().trim().parse()
}

#[test]
fn missing_card() {
    assert!(card_number("ard 42").is_err());
}

fn numbers_from_str(s: &str) -> Vec<usize> {
    let numbers: Vec<usize> = s.split(' ').filter_map(|x| x.parse().ok()).collect();
    numbers
}

#[aoc_generator(day4)]
pub fn input_generator(input: &str) -> Vec<Card> {
    input.lines().map(|s| s.parse().unwrap()).collect()
}

#[aoc(day4, part1)]
fn part_one(v: &[Card]) -> u32 {
    v.iter().map(|card| card.score()).sum()
}

#[aoc(day4, part2)]
fn part_two(v: &[Card]) -> usize {
    let mut h: HashMap<usize, usize> = HashMap::new();
    v.iter().for_each(|card| {
        h.insert(card.id, 1);
    });

    v.iter().for_each(|card| {
        card.winning_cards().for_each(|winning_card| {
            *h.get_mut(&winning_card).unwrap() += *h.get(&card.id).unwrap();
        });
    });
    h.values().sum()
}
