use itertools::Itertools;
use nom::IResult;

#[aoc_runner_derive::aoc_generator(day3)]
fn parser(s: &str) -> Vec<Symbols> {
    s.lines()
        .enumerate()
        .flat_map(|(i, x)| parse_line(x, i))
        .collect_vec()
}

#[aoc_runner_derive::aoc(day3, part1)]
fn part_one(_s: &[Symbols]) -> usize {
    0
}

fn parse_line(s: &str, line_index: usize) -> Vec<Symbols> {
    let (_, v) = digits_and_dots(s).unwrap();

    let mut lengths: Vec<usize> = Vec::new();
    for item in 0..v.len() {
        let (spaces, _) = v[item];
        lengths.push(
            spaces
                + lengths.iter().sum::<usize>()
                + if item > 0 {
                    v.get(item - 1).map(|(_, s)| s.len()).unwrap_or(0)
                } else {
                    0
                },
        );
    }

    v.iter()
        .map(|(_, x)| x)
        .zip(lengths)
        .map(|(s, spaces)| {
            if s.chars().take(1).next().unwrap().is_alphanumeric() {
                Symbols::Number(Number {
                    value: s.parse().unwrap_or_else(|_| {
                        panic!("{}", s);
                    }),
                    location: (line_index, spaces),
                })
            } else {
                Symbols::Symbol(Symbol {
                    value: s.chars().take(1).next().unwrap(),
                    location: (line_index, spaces),
                })
            }
        })
        .collect_vec()
}

fn digits_and_dots(s: &str) -> IResult<&str, Vec<(usize, &str)>> {
    nom::multi::many0(nom::sequence::pair(
        nom::multi::many0_count(nom::character::complete::char('.')),
        not_dot,
    ))(s)
}

fn not_dot(s: &str) -> IResult<&str, &str> {
    nom::bytes::complete::is_not(".")(s)
}

#[allow(dead_code)]
#[derive(Debug, PartialEq)]
enum Symbols {
    Number(Number),
    Symbol(Symbol),
}

#[allow(dead_code)]
#[derive(Debug, PartialEq)]
struct Number {
    value: usize,
    location: (usize, usize),
}

#[allow(dead_code)]
#[derive(Debug, PartialEq)]
struct Symbol {
    value: char,
    location: (usize, usize),
}
#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;

    #[rstest]
    #[case("..#..?", vec![(2, "#"), (2, "?")])]
    #[case("#..?", vec![(0, "#"), (2, "?")])]
    #[case("#..?..", vec![(0, "#"), (2, "?")])]
    #[case("..1234..?..", vec![(2, "1234"), (2, "?")])]
    fn test_digits_and_dots(#[case] input: &str, #[case] output: Vec<(usize, &str)>) {
        assert_eq!(digits_and_dots(input).unwrap().1, output);
    }

    #[rstest]
    #[case("..#..?", vec![Symbols::Symbol(Symbol{value: '#', location: (0, 2)}), Symbols::Symbol(Symbol{value: '?', location: (0, 5)})])]
    #[case("..12..?", vec![Symbols::Number(Number{value: 12, location: (0, 2)}), Symbols::Symbol(Symbol{value: '?', location: (0, 6)})])]
    fn test_parse_line(#[case] input: &str, #[case] output: Vec<Symbols>) {
        assert_eq!(parse_line(input, 0), output);
    }

    #[rstest]
    #[case("?", "?")]
    #[case("1234", "1234")]
    fn test_digit_or_not_dot(#[case] input: &str, #[case] output: &str) {
        assert_eq!(not_dot(input).unwrap().1, output);
    }

    #[rstest]
    #[case(".")]
    fn test_digit_or_not_dot_errors(#[case] input: &str) {
        assert!(not_dot(input).is_err());
    }
}
