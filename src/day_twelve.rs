use itertools::Itertools;
use rayon::prelude::*;
use std::str::FromStr;

use aoc_runner_derive::{aoc, aoc_generator};
#[aoc_generator(day12)]
fn input_generator(input: &str) -> Vec<Springs> {
    input
        .lines()
        .map(|s| s.parse())
        .collect::<Result<Vec<Springs>, SpringError>>()
        .unwrap_or_else(|x| panic!("{}", x.msg))
}

#[aoc(day12, part1)]
fn part_one(springs: &[Springs]) -> usize {
    springs.par_iter().map(|s| s.number_of_matches()).sum()
}

#[aoc(day12, part2)]
fn part_two(springs: &[Springs]) -> usize {
    springs
        .par_iter()
        .map(|s| s.into())
        .map(|s: Springs<5>| s.number_of_matches())
        .sum()
}

impl<const N: usize> Springs<N> {
    fn number_of_matches(&self) -> usize {
        number_of_matches(&self.spring_map, &self.broken_groups)
    }
}

#[derive(Debug)]
struct Springs<const N: usize = 1> {
    spring_map: String,
    broken_groups: Vec<usize>,
}

impl<const N: usize> From<&Springs<1>> for Springs<N> {
    fn from(s: &Springs<1>) -> Self {
        Self {
            spring_map: std::iter::repeat(&s.spring_map).take(N).join("?"),
            broken_groups: std::iter::repeat(&s.broken_groups)
                .take(N)
                .flatten()
                .copied()
                .collect_vec(),
        }
    }
}

#[derive(Debug)]
struct SpringError {
    msg: String,
}

impl FromStr for Springs {
    type Err = SpringError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let ss: (&str, &str) = s.split(' ').collect_tuple().ok_or(SpringError {
            msg: String::from("splitting line failed"),
        })?;
        Ok(Springs {
            spring_map: String::from(ss.0),
            broken_groups: ss
                .1
                .split(',')
                .map(|x| {
                    x.parse().map_err(|_| SpringError {
                        msg: String::from("Parsing groups failed"),
                    })
                })
                .collect::<Result<Vec<usize>, SpringError>>()?,
        })
    }
}

fn matches(s: &str, groups: &[usize]) -> bool {
    s.replace('.', " ")
        .split_ascii_whitespace()
        .map(|s| s.len())
        .collect_vec()
        == groups
}

fn cannot_match(s: &str, groups: &[usize]) -> bool {
    let up_to_question = s.split('?').take(1).next().unwrap_or("");
    let lengths = up_to_question.replace('.', " ");
    let lengths = lengths.split_ascii_whitespace().map(|s| s.len());
    lengths.zip(groups.iter()).any(|(x, y)| &x > y)
}

fn number_of_matches(s: &str, groups: &[usize]) -> usize {
    if cannot_match(s, groups) {
        return 0;
    }
    if s.contains('?') {
        number_of_matches(&s.replacen('?', "#", 1), groups)
            + number_of_matches(&s.replacen('?', ".", 1), groups)
    } else {
        matches(s, groups) as usize
    }
}

#[test]
fn simple_match() {
    assert!(matches("#", &[1]));
}

#[test]
fn simple_failed_match() {
    assert!(!matches(".", &[1]));
}

#[test]
fn two_groups() {
    assert!(matches("#.#", &[1, 1]));
}

#[test]
fn unknown() {
    assert_eq!(number_of_matches("?", &[1]), 1);
}

#[test]
fn two_unknown() {
    assert_eq!(number_of_matches("??", &[1]), 2);
}

#[test]
fn four_unknown() {
    assert_eq!(number_of_matches("??.??", &[1, 1]), 4);
}

#[test]
fn long_input() {
    assert_eq!(
        number_of_matches(
            "???.###????.###????.###????.###????.###",
            &[1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3]
        ),
        1
    );
}
/*
#[test]
fn example_part1() {
    assert_eq!(part_one(&input_generator("???.### 1,1,3\n.??..??...?##. 1,1,3\n?#?#?#?#?#?#?#? 1,3,1,6\n????.#...#... 4,1,1\n????.######..#####. 1,6,5\n?###???????? 3,2,1")), 21);
}

#[test]
fn example_part2() {
    assert_eq!(part_two(&input_generator("???.### 1,1,3\n.??..??...?##. 1,1,3\n?#?#?#?#?#?#?#? 1,3,1,6\n????.#...#... 4,1,1\n????.######..#####. 1,6,5\n?###???????? 3,2,1")), 525152);
}
*/
