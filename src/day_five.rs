use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use nom::Parser;

#[aoc_generator(day5)]
fn take_input(s: &str) -> (Vec<usize>, Vec<Vec<Range>>) {
    parse_input(s).expect("valid parsing").1
}

#[aoc(day5, part1)]
fn part_one(v: &(Vec<usize>, Vec<Vec<Range>>)) -> usize {
    let (seeds, mappings) = v;
    process_seeds(seeds.iter().copied(), mappings)
}

#[aoc(day5, part2)]
fn part_two(v: &(Vec<usize>, Vec<Vec<Range>>)) -> usize {
    let (seeds, _mappings) = v;

    let chunks: Vec<&[usize]> = seeds.chunks(2).collect();
    let iter = chunks.iter().flat_map(|&x| x[0]..x[0] + x[1]);

    process_seeds(iter, _mappings)
}

fn process_seeds<T: Iterator<Item = usize> + Send + Sync>(
    seeds: T,
    mappings: &[Vec<Range>],
) -> usize {
    seeds.map(|x| process_mappings(x, mappings)).min().unwrap()
}

fn process_mappings(seed: usize, mappings: &[Vec<Range>]) -> usize {
    mappings.iter().fold(seed, process_mapping)
}
#[allow(clippy::ptr_arg)]
fn process_mapping(seed: usize, mappings: &Vec<Range>) -> usize {
    mappings
        .iter()
        .filter_map(|range| range.try_map(seed))
        .take(1)
        .next()
        .unwrap_or(seed)
}

#[derive(Debug, PartialEq, Clone)]
struct Range {
    destination_range_start: usize,
    source_range_start: usize,
    length: usize,
}

impl Range {
    fn new(t: (usize, usize, usize)) -> Self {
        Self {
            destination_range_start: t.0,
            source_range_start: t.1,
            length: t.2,
        }
    }

    fn map(&self, s: usize) -> usize {
        s - self.source_range_start + self.destination_range_start
    }

    fn contains(&self, seed: usize) -> bool {
        (seed >= self.source_range_start) && (seed < (self.source_range_start + self.length))
    }

    fn try_map(&self, seed: usize) -> Option<usize> {
        if self.contains(seed) {
            Some(self.map(seed))
        } else {
            None
        }
    }
}

impl PartialEq<(usize, usize, usize)> for Range {
    fn eq(&self, other: &(usize, usize, usize)) -> bool {
        Range::new(*other) == *self
    }
}

fn range(s: &str) -> nom::IResult<&str, Range> {
    nom::multi::separated_list1(
        nom::character::complete::space1,
        nom::character::complete::digit1,
    )
    .map(|s| {
        s.iter()
            .map(|s: &&str| s.parse::<usize>().unwrap())
            .collect_tuple()
            .unwrap()
    })
    .map(Range::new)
    .parse(s)
}

fn ranges(s: &str) -> nom::IResult<&str, Vec<Range>> {
    nom::multi::separated_list1(nom::character::complete::newline, range)(s)
}

fn mappings(s: &str) -> nom::IResult<&str, Vec<Vec<Range>>> {
    nom::multi::separated_list1(nom::character::complete::newline, mapping)(s)
}

fn mapping(s: &str) -> nom::IResult<&str, Vec<Range>> {
    nom::sequence::preceded(
        nom::sequence::pair(
            nom::bytes::complete::take_until("map"),
            nom::bytes::complete::tag("map:\n"),
        ),
        ranges,
    )(s)
}

fn seeds(s: &str) -> nom::IResult<&str, Vec<usize>> {
    nom::sequence::preceded(
        nom::bytes::complete::tag("seeds: "),
        nom::multi::separated_list1(
            nom::character::complete::space1,
            nom::character::complete::digit1,
        ),
    )
    .map(|x| {
        x.iter()
            .map(|s: &&str| s.parse::<usize>().unwrap())
            .collect()
    })
    .parse(s)
}

fn parse_input(s: &str) -> nom::IResult<&str, (Vec<usize>, Vec<Vec<Range>>)> {
    nom::sequence::separated_pair(seeds, nom::character::complete::newline, mappings)(s)
}

#[test]
fn parse_seeds() {
    assert_eq!(seeds("seeds: 79 14 55 13").unwrap().1, vec!(79, 14, 55, 13));
}
#[test]
fn parse_range() {
    assert_eq!(range("50 98 2").unwrap().1, (50, 98, 2));
}

#[test]
fn parse_two_ranges() {
    assert_eq!(
        ranges("50 98 2\n11 12 13").unwrap().1,
        vec![(50, 98, 2), (11, 12, 13)]
    );
}

#[test]
fn what_happens_when_there_is_an_empty_line() {
    assert_eq!(
        ranges("50 98 2\n11 12 13\n\n1 2 3").unwrap().1,
        vec![(50, 98, 2), (11, 12, 13)]
    );
}

#[test]
fn seed_to_soil_map() {
    assert_eq!(
        mapping("seed-to-soil map:\n50 98 2\n11 12 13").unwrap().1,
        vec![(50, 98, 2), (11, 12, 13)]
    );
}

#[test]
fn soil_to_fertilizer_map() {
    assert_eq!(
        mapping("soil-to-fertilizer map:\n50 98 2\n11 12 13")
            .unwrap()
            .1,
        vec![(50, 98, 2), (11, 12, 13)]
    );
}

#[test]
fn multiple_mappings() {
    assert_eq!(
        mappings("seed-to-soil map:\n50 98 2\n11 12 13\n\nsoil-to-fertilizer map:\n1 2 3")
            .unwrap()
            .1,
        vec![vec![(50, 98, 2), (11, 12, 13)], vec![(1, 2, 3)]]
    );
}

#[test]
fn input_test() {
    assert_eq!(
        parse_input("seeds: 1 2 3\n\nseed-to-soil map:\n50 98 2\n11 12 13\n\nsoil-to-fertilizer map:\n1 2 3")
            .unwrap()
            .1,
            (vec![1,2,3], vec![vec![Range::new((50, 98, 2)), Range::new((11, 12, 13))], vec![Range::new((1, 2, 3))]])
    );
}
