use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;

#[aoc_generator(day11)]
fn take_input(s: &str) -> Vec<Galaxy> {
    s.lines()
        .enumerate()
        .flat_map(|(idx, x)| x.match_indices('#').map(move |(c, _)| Galaxy::new(idx, c)))
        .collect_vec()
}

#[aoc(day11, part1)]
fn part_one(v: &[Galaxy]) -> usize {
    shortest_distance(v, 1)
}

#[aoc(day11, part2)]
fn part_two(v: &[Galaxy]) -> usize {
    shortest_distance(v, 999999)
}

fn shortest_distance(v: &[Galaxy], amount: usize) -> usize {
    let widths: Vec<usize> = into_missing(v.iter().map(|x| x.width()).unique().sorted().collect());
    let heights: Vec<usize> =
        into_missing(v.iter().map(|x| x.height()).unique().sorted().collect());

    v.iter()
        .for_each(|x| x.adjust_location(&heights, &widths, amount));

    v.iter()
        .combinations(2)
        .map(|x| x[0].distance(x[1])) //, &heights, &widths))
        .sum::<usize>()
}

fn into_missing(v: Vec<usize>) -> Vec<usize> {
    let mut missing = Vec::new();
    for present in 0..v.len() - 1 {
        missing.push(v[present] + 1..v[present + 1]);
    }
    missing
        .into_iter()
        .flat_map(|x| x.collect::<Vec<usize>>())
        .collect()
}

#[test]
fn simple_example() {
    assert_eq!(into_missing(vec![1, 2, 4]), vec![3]);
}

#[test]
fn simple_example2() {
    assert_eq!(into_missing(vec![0, 2, 4, 7]), vec![1, 3, 5, 6]);
}

#[derive(Debug, PartialEq)]
struct Galaxy {
    height: std::cell::Cell<usize>,
    width: std::cell::Cell<usize>,
}

impl Galaxy {
    fn new(height: usize, width: usize) -> Self {
        Self {
            height: std::cell::Cell::new(height),
            width: std::cell::Cell::new(width),
        }
    }
    fn height(&self) -> usize {
        self.height.get()
    }

    fn width(&self) -> usize {
        self.width.get()
    }

    fn adjust_location(&self, heights: &[usize], widths: &[usize], amount: usize) {
        self.height.replace(
            self.height.get() + (heights.iter().filter(|&x| x < &self.height()).count() * amount),
        );
        self.width.replace(
            self.width.get() + (widths.iter().filter(|&x| x < &self.width()).count() * amount),
        );
    }

    fn distance(&self, other: &Galaxy) -> usize {
        self.width().abs_diff(other.width()) + self.height().abs_diff(other.height())
    }
}
macro_rules! example{
    ($name: ident, $e: expr, $f: expr)=>{

        #[test]
        fn $name() {
            let input = "...#......\n.......#..\n#.........\n..........\n......#...\n.#........\n.........#\n..........\n.......#..\n#...#.....";
            let galaxies = take_input(input);
            assert_eq!(shortest_distance(&galaxies, $e), $f);
        }
    };
}

example!(test_example_with_one, 1, 374);
example!(test_example_with_ten, 9, 1030);
example!(test_example_with_one_hundred, 99, 8410);

#[test]
fn one_galaxy_at_the_end() {
    let input = "...#";
    let galaxies = take_input(input);
    assert_eq!(galaxies, vec![Galaxy::new(0, 3)]);
}

#[test]
fn two_galaxies_at_the_end() {
    let input = "#...#";
    let galaxies = take_input(input);
    assert_eq!(part_one(&galaxies), 7);
}

#[test]
fn two_hashes_above() {
    let input = "#\n.\n#";
    let galaxies = dbg!(take_input(input));
    assert_eq!(part_one(&galaxies), 3);
}
