use std::fmt::Display;

use itertools::Itertools;
use ndarray::prelude::*;

struct DigSite(Array2<usize>);

#[aoc_runner_derive::aoc_generator(day18, part1)]
fn parse(s: &str) -> Vec<Direction> {
    s.lines()
        .map(|x| x.split(' ').collect_tuple().unwrap())
        .map(|(direction, amount, _)| match direction {
            "U" => Direction::Up(amount.parse().unwrap()),
            "D" => Direction::Down(amount.parse().unwrap()),
            "L" => Direction::Left(amount.parse().unwrap()),
            "R" => Direction::Right(amount.parse().unwrap()),
            _ => panic!(),
        })
        .collect_vec()
}

#[aoc_runner_derive::aoc_generator(day18, part2)]
fn parse2(s: &str) -> Vec<Direction> {
    s.lines()
        .map(|x| x.split(' ').collect_tuple().unwrap())
        .map(|(_, _, hex)| direction_from_hex(hex))
        .collect_vec()
}

fn direction_from_hex(hex: &str) -> Direction {
    let hex = hex.strip_prefix("(#").unwrap();
    let hex = hex.strip_suffix(')').unwrap();
    let hex = usize::from_str_radix(hex, 16).unwrap();
    let direction = hex % 16;
    let amount = ((hex) >> 4) as isize;
    match direction {
        0 => Direction::Right(amount),
        1 => Direction::Down(amount),
        2 => Direction::Left(amount),
        3 => Direction::Up(amount),
        _ => unreachable!(),
    }
}

#[aoc_runner_derive::aoc(day18, part1)]
fn part_one(v: &[Direction]) -> usize {
    let mut dug = dig(v);
    fill((2, 71), &mut dug); // would love to automate this bit
    dug.sum()
}

#[aoc_runner_derive::aoc(day18, part2)]
fn part_two(v: &[Direction]) -> usize {
    let dug = dig(v);
    //fill((2, 71), &mut dug); // would love to automate this bit
    dug.sum()
}

impl Display for DigSite {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let (height, _) = image_shape(&self.0);
        for line in (0..height).map(|x| self.0.slice(s![x, ..])) {
            for item in line.iter() {
                if item == &1 {
                    write!(f, "#")?;
                } else {
                    write!(f, ".")?;
                }
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

fn flood_fill(x: usize, y: usize, image_data: &mut Array2<usize>) {
    let target = 0usize;
    let replacement = 1usize;

    let (height, width) = image_shape(image_data);

    if image_data[[x, y]] == target {
        image_data[[x, y]] = replacement;

        if y > 0 {
            flood_fill(x, y - 1, image_data);
        }
        if x > 0 {
            flood_fill(x - 1, y, image_data);
        }
        if y < width {
            flood_fill(x, y + 1, image_data);
        }
        if x < height {
            flood_fill(x + 1, y, image_data);
        }
    }
}

fn image_shape(image_data: &Array2<usize>) -> (usize, usize) {
    let (height, width) = image_data.shape().iter().cloned().collect_tuple().unwrap();
    (height, width)
}
fn fill(location: (usize, usize), arr: &mut Array2<usize>) {
    flood_fill(location.0, location.1, arr);
}

fn dig(instructions: &[Direction]) -> Array2<usize> {
    let mut diggers = vec![(0, 0)];

    instructions
        .iter()
        .for_each(|instruction| diggers.extend(new_digger(instruction, diggers.last().unwrap())));

    let minx = diggers.iter().min_by_key(|(x, _)| x).unwrap().0;
    let miny = diggers.iter().min_by_key(|(_, y)| y).unwrap().1;

    let diggers = diggers
        .iter()
        .map(|(x, y)| (x - minx, y - miny))
        .collect_vec();

    let maxx = diggers.iter().max_by_key(|(x, _)| x).unwrap().0;
    let maxy = diggers.iter().max_by_key(|(_, y)| y).unwrap().1;

    let mut dig_site = Array2::<usize>::zeros((maxx as usize + 1, maxy as usize + 1));

    diggers.iter().for_each(|digger| {
        dig_site[[digger.0 as usize, digger.1 as usize]] = 1;
    });
    dig_site
}

fn new_digger(instruction: &Direction, location: &(isize, isize)) -> Vec<(isize, isize)> {
    match instruction {
        Direction::Up(n) => many_instructions(location, *n as usize, |x| (-(x as isize), 0)),
        Direction::Down(n) => many_instructions(location, *n as usize, |x| (x as isize, 0)),
        Direction::Left(n) => many_instructions(location, *n as usize, |x| (0, -(x as isize))),
        Direction::Right(n) => many_instructions(location, *n as usize, |x| (0, x as isize)),
    }
}

fn many_instructions(
    location: &(isize, isize),
    amount: usize,
    f: fn(usize) -> (isize, isize),
) -> Vec<(isize, isize)> {
    (0..=amount)
        .map(f)
        .map(|(x, y)| (x + location.0, y + location.1))
        .collect_vec()
}

#[derive(PartialEq, Debug)]
enum Direction {
    Up(isize),
    Down(isize),
    Left(isize),
    Right(isize),
}
#[test]
fn point() {
    assert_eq!(dig(&[]), arr2(&[[1]]));
}

#[test]
fn one_dig_right() {
    assert_eq!(dig(&[Direction::Right(1)]), arr2(&[[1, 1]]));
}

#[test]
fn one_dig_down() {
    assert_eq!(dig(&[Direction::Down(1)]), arr2(&[[1], [1]]));
}

#[test]
fn one_dig_left() {
    assert_eq!(dig(&[Direction::Left(1)]), arr2(&[[1, 1]]));
}

#[test]
fn two_dig_up() {
    assert_eq!(dig(&[Direction::Up(2)]), arr2(&[[1], [1], [1]]));
}

#[test]
fn two_dig_left() {
    assert_eq!(dig(&[Direction::Left(2)]), arr2(&[[1, 1, 1]]));
}

#[test]
fn square() {
    assert_eq!(
        dig(&[
            Direction::Up(1),
            Direction::Right(1),
            Direction::Down(1),
            Direction::Left(1)
        ]),
        arr2(&[[1, 1], [1, 1]])
    );
}

#[test]
fn filled_square() {
    let mut dig_site = dig(&[
        Direction::Up(2),
        Direction::Right(2),
        Direction::Down(2),
        Direction::Left(2),
    ]);
    fill((1, 1), &mut dig_site);
    assert_eq!(dig_site, arr2(&[[1, 1, 1], [1, 1, 1], [1, 1, 1]]));
}

#[test]
fn display() {
    let dig_site = dig(&[
        Direction::Up(2),
        Direction::Right(2),
        Direction::Down(2),
        Direction::Left(2),
    ]);
    println!("{}", DigSite(dig_site));
}

#[test]
fn example_instructions() {
    let instructions = vec![
        Direction::Right(6),
        Direction::Down(5),
        Direction::Left(2),
        Direction::Down(2),
        Direction::Right(2),
        Direction::Down(2),
        Direction::Left(5),
        Direction::Up(2),
        Direction::Left(1),
        Direction::Up(2),
        Direction::Right(2),
        Direction::Up(3),
        Direction::Left(2),
        Direction::Up(2),
    ];
    let mut dug = dig(&instructions);
    fill((1, 1), &mut dug);
    //println!("{}", DigSite(dug));
    assert_eq!(dug.sum(), 62);
}

#[test]
fn example_parsing() {
    let input = "R 6 (#70c710)\nD 5 (#0dc571)\nL 2 (#5713f0)\nD 2 (#d2c081)\nR 2 (#59c680)\nD 2 (#411b91)\nL 5 (#8ceee2)\nU 2 (#caa173)\nL 1 (#1b58a2)\nU 2 (#caa171)\nR 2 (#7807d2)\nU 3 (#a77fa3)\nL 2 (#015232)\nU 2 (#7a21e3)";
    let mut dug = dig(&parse(input));
    fill((1, 1), &mut dug);
    assert_eq!(dug.sum(), 62);
}

#[test]
fn string_from_hex() {
    assert_eq!(direction_from_hex("(#000010)"), Direction::Right(1));
    assert_eq!(direction_from_hex("(#70c710)"), Direction::Right(461937));
    assert_eq!(direction_from_hex("(#7807d2)"), Direction::Left(491645));
}
