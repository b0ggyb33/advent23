use itertools::Itertools;

use aoc_runner_derive::{aoc, aoc_generator};
#[aoc_generator(day7)]
fn input_generator(input: &str) -> Vec<(Hand, usize)> {
    input
        .lines()
        .map(|s| s.split(' ').collect_tuple().unwrap())
        .map(|(hand, bet)| (hand.parse().unwrap(), bet.parse().unwrap()))
        .collect_vec()
}

#[aoc(day7, part1)]
fn part_one(input: &[(Hand, usize)]) -> usize {
    input
        .iter()
        .sorted_by_key(|x| x.0)
        .enumerate()
        .map(|(idx, x)| x.1 * (1 + idx))
        .sum()
}

use std::str::FromStr;

#[derive(Debug, Eq, PartialEq, PartialOrd, Ord, Copy, Clone)]
enum Hand {
    HighCard([Card; 5]),
    Pair([Card; 5]),
    TwoPair([Card; 5]),
    ThreeOfAKind([Card; 5]),
    FullHouse([Card; 5]),
    FourOfAKind([Card; 5]),
    FiveOfAKind([Card; 5]),
}

#[derive(Hash, Debug, Eq, PartialEq, PartialOrd, Ord, Copy, Clone)]
enum Card {
    Two = 2,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}

#[test]
fn card_ordering() {
    assert!(Card::Two < Card::Three);
    assert!(Card::Three < Card::Four);
    assert!(Card::Four < Card::Five);
    assert!(Card::Five < Card::Six);
    assert!(Card::Nine < Card::Ten);
    assert!(Card::Ten < Card::Jack);
    assert!(Card::Jack < Card::Queen);
    assert!(Card::Queen < Card::King);
}

#[test]
fn hand_ordering() {
    assert!(
        Hand::HighCard([Card::Ace, Card::Two, Card::Three, Card::Four, Card::Five])
            > Hand::HighCard([Card::King, Card::Two, Card::Three, Card::Four, Card::Five])
    );

    assert!(
        Hand::HighCard([Card::King, Card::Three, Card::Three, Card::Four, Card::Five])
            > Hand::HighCard([Card::King, Card::Two, Card::Three, Card::Four, Card::Five])
    );

    assert!(
        Hand::Pair([
            Card::Queen,
            Card::Queen,
            Card::Three,
            Card::Four,
            Card::Five
        ]) > Hand::HighCard([Card::King, Card::Two, Card::Three, Card::Four, Card::Five])
    );
}

#[derive(Debug)]
struct CardError;

impl TryFrom<char> for Card {
    type Error = CardError;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '2' => Ok(Card::Two),
            '3' => Ok(Card::Three),
            '4' => Ok(Card::Four),
            '5' => Ok(Card::Five),
            '6' => Ok(Card::Six),
            '7' => Ok(Card::Seven),
            '8' => Ok(Card::Eight),
            '9' => Ok(Card::Nine),
            'T' => Ok(Card::Ten),
            'J' => Ok(Card::Jack),
            'Q' => Ok(Card::Queen),
            'K' => Ok(Card::King),
            'A' => Ok(Card::Ace),
            _ => Err(Self::Error {}),
        }
    }
}

impl FromStr for Hand {
    type Err = CardError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let hand: [Card; 5] = s
            .chars()
            .map(|c| c.try_into().unwrap())
            .collect_vec()
            .try_into()
            .unwrap();
        match duplicates(&hand) {
            (5, _) => Ok(Hand::FiveOfAKind(hand)),
            (4, _) => Ok(Hand::FourOfAKind(hand)),
            (3, 2) => Ok(Hand::FullHouse(hand)),
            (3, _) => Ok(Hand::ThreeOfAKind(hand)),
            (2, 2) => Ok(Hand::TwoPair(hand)),
            (2, _) => Ok(Hand::Pair(hand)),
            _ => Ok(Hand::HighCard(hand)),
        }
    }
}

fn duplicates(cards: &[Card]) -> (usize, usize) {
    let sorted_values = cards.iter().counts();
    let values = sorted_values.values().sorted().rev().collect_vec();
    dbg!(&values);
    if values.len() == 1 {
        (5, 0)
    } else {
        values[0..=1]
            .iter()
            .cloned()
            .cloned()
            .collect_tuple()
            .unwrap()
    }
}

#[test]
fn hand_strings() {
    assert_eq!(
        "AAAAA".parse::<Hand>().unwrap(),
        Hand::FiveOfAKind([Card::Ace, Card::Ace, Card::Ace, Card::Ace, Card::Ace,])
    );

    assert_eq!(
        "AAKAA".parse::<Hand>().unwrap(),
        Hand::FourOfAKind([Card::Ace, Card::Ace, Card::King, Card::Ace, Card::Ace,])
    );

    assert_eq!(
        "AKKAK".parse::<Hand>().unwrap(),
        Hand::FullHouse([Card::Ace, Card::King, Card::King, Card::Ace, Card::King,])
    );

    assert_eq!(
        "AKK2K".parse::<Hand>().unwrap(),
        Hand::ThreeOfAKind([Card::Ace, Card::King, Card::King, Card::Two, Card::King,])
    );

    assert_eq!(
        "AKK2A".parse::<Hand>().unwrap(),
        Hand::TwoPair([Card::Ace, Card::King, Card::King, Card::Two, Card::Ace,])
    );

    assert_eq!(
        "3KK2A".parse::<Hand>().unwrap(),
        Hand::Pair([Card::Three, Card::King, Card::King, Card::Two, Card::Ace,])
    );

    assert_eq!(
        "23456".parse::<Hand>().unwrap(),
        Hand::HighCard([Card::Two, Card::Three, Card::Four, Card::Five, Card::Six,])
    );
}
